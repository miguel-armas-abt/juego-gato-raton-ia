package view;

import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class AcercaDe extends JFrame 
{
	private Principal principal;
	private JLabel label1,label2,label3;	
	private JTextArea area;	
	private JScrollPane panel;	
	
	public AcercaDe(Principal Pprincipal) 
        {
		principal = Pprincipal;
		iniciar();
		alinear();
		setSize(500,350);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
		setLocationRelativeTo(principal);
	}
	
	public void alinear() 
        {            
		setLayout(null);
		label1.setBounds(160,0,500,50);
		getContentPane().add(label1);
		label2.setBounds(0,250,500,50);
		getContentPane().add(label2);

		panel.setBounds(0,50,485,200);
		getContentPane().add(panel);
	}        
	
	public void iniciar() 
        {
		label1 = new JLabel("LOS GATOS Y EL RATÓN");
		label1.setFont(new Font("courier new", Font.CENTER_BASELINE, 18));
		label2 = new JLabel("Version 1.0");
		label2.setFont(new Font("courier new", Font.CENTER_BASELINE, 13));
		label3 = new JLabel("Fuentes:");
		label2.setFont(new Font("courier new", Font.CENTER_BASELINE, 13));
        
		area = new JTextArea();
		area.setEditable(false);
		area.setText(
                        "FACULTAD DE INGENIERÍA DE SISTEMAS E INFORMÁTICA, UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS\n"
                                + "Realizado por Miguel Alderete, Miguel Armas, Edinson Boada y Yanpieer Romero \n"
				+ "Este programa es una simulación del popular juego 'Los gatos y el ratón'\n\n"
                                + "que utiliza tópicos de Inteligencia Artificial. Para ello se empleó el\n"
				+ "lenguaje de programación Java, el cual mediante la librería ABCL permitió\n"
				+ "la conexión con el lenguaje CommonLisp.\n\n"
				+ "Esta simulación tiene todos los modos de juego convencionales y se encuentra"
				+ "en su fase beta, pues aún falta definir alguna heurística.\n\n"
				+ "Expuesto el 7 de octubre de 2019");
		panel = new JScrollPane(area);
	}
	
}