package models;

public class Ficha 
{
    private int x;  // coordenada x
    private int y;  // coordenada y

    public Ficha() {
        this.x = 0;
        this.y = 0;
    }
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
