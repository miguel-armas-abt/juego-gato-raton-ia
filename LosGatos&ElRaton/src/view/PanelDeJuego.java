package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import lisp.LispCore;
import javax.swing.*;
import mundo.GyR;

@SuppressWarnings("serial")
public class PanelDeJuego extends JPanel implements ActionListener {

    LispCore CL = new lisp.LispCore();
    private JButton boton[][];
    private GyR GR;
    private PanelImagen imagen;
    private NuevaPartida sele;
    private int tamanio = 60, tablero[][] = new int[8][8];
    static int humanox, humanoy;

    public void iniciar() 
    {
        imagen = new PanelImagen();
        boton = new JButton[8][8];
        GR = new GyR();

        removeAll();
        for (int i = 0; i < boton.length; i++) {
            for (int j = 0; j < boton[0].length; j++) {

                boton[i][j] = new JButton();
                boton[i][j].setBackground(Color.WHITE);
                boton[i][j].setBorderPainted(false);
                boton[i][j].setContentAreaFilled(false);
                boton[i][j].setOpaque(false);
                boton[i][j].addActionListener(this);
                boton[i][j].setIcon(new ImageIcon("./imagenes/EspacionBlancoB.png"));
            }
        }
    }

    public void ResetearTablero() 
    {
        GR.poner_fichas();
        ImageIcon icon = new ImageIcon();
        String[] options = {"Gatos", "Raton"};
        int seleccion = JOptionPane.showOptionDialog(null, "¿Que jugador empieza?",
                                                    "Titulo", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, icon , options, options[0]);       
        if(seleccion == 0)
        {
            GR.color = 'R';
        }
        else
        {
            GR.color = 'N';
        }                                                                
    }

    public void dibujar_tablero() 
    {
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero.length; j++) {
                if (j % 2 == 0) {
                    tablero[1][j] = 5;
                    tablero[7][j] = 5;
                    tablero[5][j] = 5;
                    tablero[3][j] = 5;
                } else {
                    tablero[0][j] = 5;
                    tablero[6][j] = 5;
                    tablero[2][j] = 5;
                    tablero[4][j] = 5;
                }
            }
        }
    }

    public void crear_tabero() 
    {
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < boton.length; j++) {
                if (tablero[i][j] == GR.getRaton()) {
                    boton[i][j].setIcon(new ImageIcon("./imagenes/jerrys.png"));
                } else if (tablero[i][j] == GR.getGatos()) {
                    boton[i][j].setIcon(new ImageIcon("./imagenes/toms.png"));
                } else if (tablero[i][j] == GR.getRelleno()) {
                    boton[i][j].setIcon(new ImageIcon("./imagenes/EspacionNegroN.png"));
                }
            }
        }
    }

    public void dibujar_en_los_botones() 
    {
        GR.verificar(GR.getColor());
        for (int i = 0; i < boton.length; i++) {
            for (int j = 0; j < boton.length; j++) {
                if (GR.Ver(i, j) == GR.getRaton()) {
                    boton[i][j].setIcon(new ImageIcon("./imagenes/jerrys.png"));
                } else if (GR.Ver(i, j) == GR.getGatos()) {
                    boton[i][j].setIcon(new ImageIcon("./imagenes/toms.png"));
                } else if (GR.Ver(i, j) == GR.getRelleno()) {
                    boton[i][j].setIcon(new ImageIcon("./imagenes/EspacionNegroN.png"));
                }
            }
        }
    }

    public void alo() 
    {
        GR.poner_fichas();
        dibujar_en_los_botones();
    }

    public void IA(boolean pasa, String func, int j) 
    {
        if(j == 1)
        {
            System.out.println("El gato está pensando .. ");
        }
        else
            if( j == 0 )
            {
                System.out.println("El ratón está pensando .. ");
            }
        
        char[] lista = CL.lispFunctionWithElevenParamsInt(func,
                GR.gatosV[0].getY(), Transformar(GR.gatosV[0].getX()),
                GR.gatosV[1].getY(), Transformar(GR.gatosV[1].getX()),
                GR.gatosV[2].getY(), Transformar(GR.gatosV[2].getX()),
                GR.gatosV[3].getY(), Transformar(GR.gatosV[3].getX()),
                GR.ratonV.getY(), Transformar(GR.ratonV.getX()), j
        );                       

        int x = Transformar(Integer.parseInt(Character.toString(lista[1])));
        int y = Integer.parseInt(Character.toString(lista[0]));
        int x1 = Transformar(Integer.parseInt(Character.toString(lista[3])));
        int y1 = Integer.parseInt(Character.toString(lista[2]));
        
        Asignar(x, y, x1, y1);

        if (GR.jugar(GR.getColor(), x, y, x1, y1) == true) 
        {
            dibujar_en_los_botones();
            boton[x][y].setIcon(new ImageIcon("./imagenes/EspacionNegroN.png"));
        }
    }

    public void alinear() 
    {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(8, 8));

        for (int i = 0; i < boton.length; i++) {
            for (int j = 0; j < boton[0].length; j++) {
                panel.add(boton[i][j]);
            }
        }
        add(panel);
    }

    public PanelDeJuego() 
    {
        sele = new NuevaPartida();
        iniciar();
        setLayout(new BorderLayout());
        setVisible(true);
        alinear();
        dibujar_tablero();
        crear_tabero();
    }

    public void cambiar(int rojas, int negras) 
    {
        this.gato = rojas;
        this.rata = negras;
    }

    int contar = 0;
    int x = 0, y = 0, x1 = 0, y1 = 0, gato, rata;

    @Override
    public void actionPerformed(ActionEvent e) 
    {
        boolean pasa = false;                                       
        for (int i = 0; i < boton.length; i++) {
            for (int j = 0; j < boton[0].length; j++) {
                if (e.getSource() == boton[i][j]) 
                {
                    	if(gato == 1 && rata == 1) 
                        {
//-------------------------------Rojas:Automatico Vs Negras:Automatico-------------------------------------------------
                            if(GR.getColor() == 'N')
                            {
                                IA(pasa, "NUEVA-POSICION-RAT", 0);
                                System.out.println("El gato jugó su turno.");
                                GR.CambioDeTurno();
                            }
       			    else
                            {   
                                IA(pasa, "NUEVA-POSICION-CHAT", 1);
                                System.out.println("El raton jugó su turno.");
                                GR.CambioDeTurno();
                            }
												
			}else if (gato == 2 && rata == 2) 
                        {   
//-------------------------------Rojas:Manual Vs Negras:Manual-------------------------------------------------
                            if (GR.verificar_exitencia_de_ficha(GR.getColor(), i, j)) 
                            {
                                x = i;
                                y = j;
                                contar++;                                
                            }
                            else if (contar == 1) 
                            {
                                if (GR.jugar(GR.getColor(), x, y, i, j)) 
                                {
                                    dibujar_en_los_botones();
                                    if(GR.getColor() == 'N')
                                    {
                                        System.out.println("El raton jugó su turno.");
                                    }
                                    else
                                    {
                                        System.out.println("El gato jugó su turno.");
                                    }
                                    
                                    GR.CambioDeTurno();
                                } 
                                else 
                                {
                                    JOptionPane.showMessageDialog(null, "Movimiento Invalido");
                                }
                                contar--;
                            }
                        }else if (gato == 1 && rata == 2) 
                        {
//-------------------------------Rojas:Automatico Vs Negras:Manual-------------------------------------------------
                            if (GR.getColor() == 'N') 
                            {
                                if (GR.verificar_exitencia_de_ficha(GR.getColor(), i, j)) 
                                {                                    
                                    x = i;
                                    y = j;
                                    contar++;
                                }
                                else if (contar == 1) 
                                {
                                    if (GR.jugar(GR.getColor(), x, y, i, j)) 
                                    {
                                        humanox = i;
                                        humanoy = j;
                                        GR.ratonV.setX(humanox);
                                        GR.ratonV.setY(humanoy);

                                        // creo un array para mis parametros
                                        ArrayList<Integer> twoParams = new ArrayList<>();
                                        twoParams.add(Transformar(humanox));
                                        twoParams.add(humanoy);
                                        CL.lispFunctionWithIntegerParams("ACTUALIZAR-TABLERO", twoParams);
                                        
                                        //CL.lispFunctionWithTwoParamsInt("ACTUALIZAR-TABLERO", Transformar(humanox), humanoy);
                                        System.out.println("El raton jugó su turno.");
                                   //GR.Verte();
                                        //              System.out.println("--x>"+Transformar(humanox));
                                        //              System.out.println("---y>"+humanoy);
                                        dibujar_en_los_botones();
                                        GR.CambioDeTurno();

                                    }else 
                                    {
                                        JOptionPane.showMessageDialog(null, "Movimiento Invalido");
                                    }
                                    contar--;
                                }
                            }
                            else {
                                    IA(pasa, "NUEVA-POSICION-CHAT", 1);
                                    System.out.println("El gato jugó su turno.");
                                    GR.CambioDeTurno();
                                }

                        } else if (gato == 2 && rata == 1) 
                        {
 //-------------------------------Rojas:Manual Vs Negras:Automatico-------------------------------------------------

                            if (GR.getColor() == 'R') 
                            {
                                if (GR.verificar_exitencia_de_ficha(GR.getColor(), i, j)) 
                                {
                                    x = i;
                                    y = j;
                                    contar++;
                                } 
                                else if (contar == 1) 
                                {
                                    if (GR.jugar(GR.getColor(), x, y, i, j)) 
                                    {

                                        humanox = i;
                                        humanoy = j;                                    
                                        Asignar(x, y, humanox, humanoy);
                                        
                                        // creo un arreglo para pasar los parametros
                                        ArrayList<Integer> fourParams = new ArrayList<>();
                                        fourParams.add(y);
                                        fourParams.add(Transformar(x));
                                        fourParams.add(humanoy);
                                        fourParams.add(Transformar(humanox));
                                        CL.lispFunctionWithIntegerParams("ACTUALIZAR-TABLEROG", fourParams);
                                        
                                        // CL.LISPcon4P("ACTUALIZAR-TABLEROG", y, Transformar(x), humanoy, Transformar(humanox));
                                        System.out.println("El gato jugó su turno.");
                                        dibujar_en_los_botones();
                                        GR.CambioDeTurno();
                                    } else 
                                    {
                                        JOptionPane.showMessageDialog(null, "Movimiento Invalido");
                                    }
                                    contar--;
                                }
                            } else 
                            {
                                IA(pasa, "NUEVA-POSICION-RAT", 0);
                                System.out.println("El raton jugó su turno.");
                                GR.CambioDeTurno();
                        }

                    }
                }
            }
        }

    }

    public int Transformar(int x) {
        x = (-1) * (x - 7);
    //    System.out.println(x);
        return x;
    }

    public void Asignar(int x, int y, int x1, int y1) {
        for (int i = 0; i < 4; i++) {
            if (GR.gatosV[i].getX() == x && GR.gatosV[i].getY() == y) {
                GR.gatosV[i].setX(x1);
                GR.gatosV[i].setY(y1);
            }

            if (GR.ratonV.getX() == x && GR.ratonV.getY() == y) {
                GR.ratonV.setX(x1);
                GR.ratonV.setY(y1);
            }
        }
    }

}
