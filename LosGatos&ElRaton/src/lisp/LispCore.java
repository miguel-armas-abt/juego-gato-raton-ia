package lisp;
import java.util.ArrayList;
import java.util.ResourceBundle;
import org.armedbear.lisp.Function;
import org.armedbear.lisp.Interpreter;
import org.armedbear.lisp.LispInteger;
import org.armedbear.lisp.LispObject;
import org.armedbear.lisp.Packages;
import org.armedbear.lisp.Symbol;

public class LispCore
{
    ResourceBundle recurso = ResourceBundle.getBundle("lisp/lisp");
    Interpreter interpreter = Interpreter.createInstance();
    
    static int cont = 0;
    static LispObject lispTwoParams[] = new LispObject[2];
    static LispObject lispFourParams[] = new LispObject[4];
        
    public String lispFunctionWithIntegerParams(String lispFunctionName, ArrayList<Integer> params)
    {
        String output = "";
        try {	
            // obtengo acceso al script de Lisp
            this.interpreter.eval("(load "+ recurso.getString("recurso") +")"); 
            org.armedbear.lisp.Package DPK = Packages.findPackage("CL-USER");
           
            // compruebo la cantidad de parametros pasados
            if (params.size() == 2 ){
                params.stream().forEach(p -> {
                    LispObject lispParam = LispInteger.getInstance(p);
                    lispTwoParams[cont] = lispParam;
                    cont++;
                });
            }
            if (params.size() == 4 ){
                params.stream().forEach(p -> {
                    LispObject lispParam = LispInteger.getInstance(p);
                    lispFourParams[cont] = lispParam;
                    cont++;
                });
            }
            cont = 0;
            
            // recupero la funcion lisp mediante su nombre
            Symbol lispSymbol = DPK.findAccessibleSymbol(lispFunctionName);
            Function lispFunction = (Function) lispSymbol.getSymbolFunction();
           
            // paso los parametros a la funcion lisp y la ejecuto
            LispObject objectReturn = null;
            if (params.size() == 2 ){
                objectReturn = lispFunction.execute(lispTwoParams);
            }
            if (params.size() == 4 ){
                objectReturn = lispFunction.execute(lispFourParams);
            }
            
            // recupero la salida de consola
            output = objectReturn.printObject();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        return output;
    }
    
    public char[] lispFunctionWithElevenParamsInt(String lispFunctionName, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int x5, int y5, int R)
    {
        String output = "";
        try
        {	
            // obtengo acceso al script de Lisp
            this.interpreter.eval("(load "+ recurso.getString("recurso") +")"); 
            org.armedbear.lisp.Package DPK = Packages.findPackage("CL-USER");
           
           // convierto los parametros a tipo Lisp
            LispObject lispParam = interpreter.eval(
                   "'(("+x1+" "+y1+")"+"("+x2+" "+y2+")"+"("+x3+" "+y3+")"+"("+x4+" "+y4+")"+"("+x5+" "+y5+")"+R+")");
           
            // recupero la funcion lisp mediante su nombre
            Symbol lispSymbol = DPK.findAccessibleSymbol(lispFunctionName);
            Function lispFunction = (Function) lispSymbol.getSymbolFunction();
            
           // paso los parametros a la funcion lisp y la ejecuto       
            LispObject objectReturn = lispFunction.execute(lispParam);
            
             // recupero la salida de consola
            output = objectReturn.printObject();
        }
       catch(Throwable t) {
           t.printStackTrace();
        }
        
        String listaLimpia = output.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(" ", "");
        char[] elementos = listaLimpia.toCharArray();
        return elementos;
    }
}     