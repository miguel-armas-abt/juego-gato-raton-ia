package mundo;

import models.Ficha;
import javax.swing.JOptionPane;

public class GyR {
    
	private int tabla[][];
        public Ficha gatosV[] =  new Ficha[4];
        public Ficha ratonV = new Ficha(); 
	private final int Gatos = 1, Raton = 2,relleno = 5;
	public char color = 'N';    
        
        public GyR()
        {
            tabla = new int[8][8];                        
        }
                       	
	public char getColor() {
		return color;
	}
	public int getRelleno() {
		return relleno;
	}
	public int getGatos() {
		return Gatos;
	}
	public int getRaton() {
		return Raton;
	}

	
	public void CambioDeTurno() //R SIMBOLIZA GATOS Y N SIMBOLIZA RATON
        {   
		if(color == 'R') 
                {
                    color = 'N';
		}else 
                {
                    color = 'R';
		}
	}


	public int Ver(int i,int j) 
        {
            posibilidad_reina();
            return tabla[i][j];
	}
	
	public void poner_fichas() 
        {	            
            int numero = (int) (Math.random() * 4);                               
		for (int j = 0; j < tabla.length; j++) 
                {
                    if (j % 2 == 0) 
                    {
        		tabla[1][j] = relleno;
			tabla[7][j] = relleno;
                        tabla[5][j] = relleno;
			tabla[3][j] = relleno;
                    } 
                    else 
                    {
			tabla[0][j] = Gatos;
                        tabla[6][j] = relleno;
			tabla[2][j] = relleno;
                        tabla[4][j] = relleno;
                    }
                }                           
        
                tabla[7][2*numero] = Raton;                       
                ratonV.setX(7);
                ratonV.setY(numero*2);                                              
                                  
                for (int i=0; i<gatosV.length;i++)
                {
                    gatosV[i]=new Ficha();
                }   
                    gatosV[0].setX(0);
                    gatosV[0].setY(1);
                    gatosV[1].setX(0);
                    gatosV[1].setY(3);
                    gatosV[2].setX(0);
                    gatosV[2].setY(5);
                    gatosV[3].setX(0);
                    gatosV[3].setY(7);                       
                    //Verte();                        
		}

	
	public boolean verificar_exitencia_de_ficha(char color, int x,int y) 
        {
		if(color == 'N') {
			if(tabla[x][y] == Raton) {
				return true;
			}
		}else if(color == 'R'){
			if(tabla[x][y] == Gatos) {
				return true;
			}
		}
		return false;
	}
	
	
	public void imprimir(int contadorN, int contadorR, char color) 
        {
	}

	public void buscar_ficha(int i,int j) 
        {
		if(tabla[i][j] == getRaton()) {
			System.out.println("Raton");
		}else if(tabla[i][j] == getGatos()){
			System.out.println("Gatos");
		}
	}
	
	public void mostrartabla() 
        {
		for (int i = 0; i < tabla.length; i++) {
			for (int j = 0; j < tabla.length; j++) {
				System.out.print(tabla[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	/**
	 * @param color
	 */
	public boolean jugar(char color, int x, int y, int x1, int y1) 
        {
                boolean pasa = false;	
                int variable, enemigo;                            
                if (color == 'R') {
			enemigo = Raton;
			variable = Gatos;

		} else {
			enemigo = Gatos;
			variable = Raton;
		}
                
		while (pasa == false) {
			
			if (tabla[x][y] == variable) 
                        {
				if (y1 > -1 && x1 > -1 && y1 < 8 && x1 < 8) //Si esta entre [0,0] y [7,7]
                                {
                                    if (tabla[x1][y1] == 5)
                                    {
					if (variable == Raton) //JUEGA EL RATON
                                        {
                                            if (x1 == x - 1)
                                            {
						if (y1 == y - 1 || y1 == y + 1) 
                                                {
                                                    tabla[x][y] = 5;
                                                    tabla[x1][y1] = Raton;
                                                    pasa = true;
						}
                                            }
                                            if (x1 >= x - 2)
                                            {
                                                
                                            }
                                            
                                            if (x1 == x + 1)
                                            {               
						if (y1 == y - 1 || y1 == y + 1) 
                                                {
                                                    tabla[x][y] = 5;
                                                    tabla[x1][y1] = Raton;
                                                    pasa = true;
						}
                                            }
                                            
                                            if (x1 >= x + 2)
                                            {
                                                
                                            }
                                               
							
					} else if (variable == Gatos) //JUEGAN LOS GATOS
                                                {
                                                    if (x1 == x + 1)  
                                                    {
							if (y1 == y - 1 || y1 == y + 1) 
                                                        {
                                                            tabla[x][y] = 5;
                                                            tabla[x1][y1] = Gatos;
                                                            pasa = true;
                                                        }
                                                    }
                                                    if (x1 >= x + 2) 
                                                    {                                                    
                                                    }
						}
                                                else
                                                {
                                                }
                                                
                                    }

				} else {
					 //JOptionPane.showMessageDialog(null, "coordenadas no validas");
				}
			}
                        if(pasa == false) 
                        {
                            break;
			}		
			}
		return pasa;
	}

	public boolean verificar(char color) 
        {            
            int contadorG[][] = new int[4][2];
            int contadorR[][] = new int [1][2];
            int k=0;                                    
            
            for (int i = 0; i < tabla.length; i++) 
            {
                for (int j = 0; j < tabla.length; j++) 
                {
                    if(tabla[i][j] == Gatos)
                    {
                        contadorG[k][0] = i;
                        contadorG[k][1] = j;
                        k++;
                    }
                    
                    if(tabla[i][j] == Raton)
                    {
                        contadorR[0][0] = i;                        
                        contadorR[0][1] = j;
                    }
                }     
            }
     
            if( contadorR[0][0] < contadorG[0][0] &&
                contadorR[0][0] < contadorG[1][0] && 
                contadorR[0][0] < contadorG[2][0] && 
                contadorR[0][0] < contadorG[3][0] )
            {
                        JOptionPane.showMessageDialog(null, "Gana el Raton!");
			return true;
            }
            
            if((tabla[7][0]==Raton && tabla[6][1]==Gatos) ||
                   (tabla[0][7]==Raton && tabla[1][6]==Gatos) )
            {
                JOptionPane.showMessageDialog(null, "Ganan los gatos!");
                return true;
            }
                        
            for (int i = 1; i < 7; i++) 
            {
                for (int j = 1; j < 7; j++) 
                {
                    if(tabla[i][j]==Raton && tabla[i+1][j+1]==Gatos && tabla[i-1][j-1]==Gatos
                            && tabla[i+1][j-1]==Gatos && tabla[i-1][j+1]==Gatos)
                    {
                        JOptionPane.showMessageDialog(null, "Ganan los gatos!");
			return true;
                    }
                }     
            }
            
            for(int i = 0; i<=6; i++)
            {
                if((tabla[i][0]==Raton && tabla[i+1][1]==Gatos && tabla[i-1][1]==Gatos) || 
                   (tabla[0][i]==Raton && tabla[1][i+1]==Gatos && tabla[1][i-1]==Gatos) ||
                   (tabla[i][7]==Raton && tabla[i+1][6]==Gatos && tabla[i-1][6]==Gatos) ||
                   (tabla[7][i]==Raton && tabla[6][i+1]==Gatos && tabla[6][i-1]==Gatos)
                   )
                {
                    JOptionPane.showMessageDialog(null, "Ganan los gatos!");
			return true;
                }
            }
            
            for(int i = 0; i<=6; i++)
            {
                if((tabla[i][0]==Raton && tabla[i+1][1]==Gatos && tabla[i-1][1]==Gatos) || (tabla[0][i]==Raton && tabla[1][i+1]==Gatos && tabla[1][i-1]==Gatos))
                {
                    JOptionPane.showMessageDialog(null, "Ganan los gatos!");
			return true;
                }
            }       
            return false;
	}

	public void posibilidad_reina() 
        {
		for (int j = 0; j < tabla[0].length; j++) {
			if (tabla[0][j] == Raton) {
				tabla[0][j] = Raton;
			} else if (tabla[7][j] == Gatos) {
				tabla[7][j] = Gatos;
			}
		}
	}
 
        public void Verte()
        {
            for (int i = 0; i < 4; i++) {
                System.out.println(gatosV[i].getX()+ "  "+gatosV[i].getY());                
            }
            System.out.println(ratonV.getX()+ "  "+ratonV.getY());                      
        }        
}
