package view;
public class PanelBienvenido extends javax.swing.JFrame 
{
    public PanelBienvenido() 
    {
        initComponents();
        transparente();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_inicio = new javax.swing.JPanel();
        jButton_empezar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel_inicio = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(100, 100));
        setMinimumSize(new java.awt.Dimension(904, 490));

        jPanel_inicio.setMaximumSize(new java.awt.Dimension(904, 472));
        jPanel_inicio.setPreferredSize(new java.awt.Dimension(904, 472));
        jPanel_inicio.setLayout(null);

        jButton_empezar.setBackground(new java.awt.Color(255, 255, 153));
        jButton_empezar.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        jButton_empezar.setForeground(new java.awt.Color(153, 255, 102));
        jButton_empezar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Play_2.png"))); // NOI18N
        jButton_empezar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton_empezar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Play.png"))); // NOI18N
        jButton_empezar.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jButton_empezar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton_empezar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_empezarActionPerformed(evt);
            }
        });
        jPanel_inicio.add(jButton_empezar);
        jButton_empezar.setBounds(700, 380, 50, 60);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_apagado.jpg"))); // NOI18N
        jButton1.setText("jButton1");
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_prendido.jpg"))); // NOI18N
        jPanel_inicio.add(jButton1);
        jButton1.setBounds(0, 0, 900, 470);

        jLabel_inicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inicio.jpg"))); // NOI18N
        jPanel_inicio.add(jLabel_inicio);
        jLabel_inicio.setBounds(0, 0, 904, 470);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel_inicio, javax.swing.GroupLayout.PREFERRED_SIZE, 895, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel_inicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_empezarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_empezarActionPerformed

        new Principal();
        dispose();
    }//GEN-LAST:event_jButton_empezarActionPerformed

    private void transparente()
    {
        jButton_empezar.setOpaque(false);
        jButton_empezar.setContentAreaFilled(false);
        jButton_empezar.setBorderPainted(false);        
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PanelBienvenido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PanelBienvenido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PanelBienvenido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PanelBienvenido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PanelBienvenido().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton_empezar;
    private javax.swing.JLabel jLabel_inicio;
    private javax.swing.JPanel jPanel_inicio;
    // End of variables declaration//GEN-END:variables
}
